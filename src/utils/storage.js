const KEY = 'shopVUE3';

export const setStorage = value => {
    localStorage.setItem(KEY, JSON.stringify(value));
};

export const getStorage = () => {
    const data = localStorage.getItem(KEY);
    if (!data) return null;

    return JSON.parse(data);
};
