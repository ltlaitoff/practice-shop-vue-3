import { createApp } from 'vue';
import { createRouter, createWebHashHistory } from 'vue-router';

import App from './components/App.vue';
import ProductInfo from './components/ProductInfo.vue';
import ProductsList from './components/ProductsList.vue';
import { store } from './store/store';

const routes = [
    { path: '/', component: ProductsList },
    { path: '/product/:id', component: ProductInfo },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

const app = createApp(App);

app.use(router);
app.use(store);

app.mount('#app');
