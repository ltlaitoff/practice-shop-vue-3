import { findElementIndexById } from './helpers';
import { DEFAULT_COUNT } from './shoppingCartStoreConfig';

export const ADD_TO_CART = (state, product) => {
    if (findElementIndexById(state.cart, product.id) > -1) return;

    const productForAdd = {
        ...product,
        count: DEFAULT_COUNT,
    };

    state.cart.push(productForAdd);
};

export const CHANGE_PRODUCT_COUNT = (state, { productId, quantity }) => {
    const productIndex = findElementIndexById(state.cart, productId);

    if (productIndex === -1) return;

    const resultCount = state.cart[productIndex].count + quantity;

    if (resultCount <= 0) {
        REMOVE_PRODUCT(state, productId);
        return;
    }

    state.cart[productIndex].count = resultCount;
};

export const REMOVE_PRODUCT = (state, productId) => {
    const productIndex = findElementIndexById(state.cart, productId);

    if (productIndex === -1) return;

    state.cart.splice(productIndex, 1);
};

export const SET_CART = (state, products) => {
    state.cart = products;
};

export const CLEAR_CART = state => {
    state.cart = [];
};
