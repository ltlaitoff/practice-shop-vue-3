export const findElementIndexById = (array, id) => {
    return array.findIndex(item => item.id === id);
};
