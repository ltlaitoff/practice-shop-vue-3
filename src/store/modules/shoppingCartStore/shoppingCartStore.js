import state from './shoppingCartStoreState';
import * as mutations from './shoppingCartStoreMutations';
import * as getters from './shoppingCartStoreGetters';

const shoppingCartStore = {
    namespaced: true,
    state,
    mutations,
    getters,
};

export { shoppingCartStore };
