import { createStore } from 'vuex';

import { shoppingCartStore } from './modules';

const store = createStore({
    modules: {
        shoppingCart: shoppingCartStore,
    },
});

export { store };
